/**
 * Vérification au chargement du site si un token existe : si oui, récupération de l'utilisateur connecté, et stockage des informations dans le store
 */

import gql from 'graphql-tag'

export default function ({ app, store }) {
  if (app.$apolloHelpers.getToken()) {
    app.apolloProvider.defaultClient
      .query({
        query: gql`
          query {
            me {
              email
              name
            }
          }
        `,
      })
      .then(({ data }) => {
        if (data.me.email) {
          store.commit('updateUser', data.me)
        }
      })
  }
}
